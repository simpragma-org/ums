var timeline = angular.module('timeline',[]);

timeline.factory('scanFactory', function() {
    var scans = [
        {
            sid: 0,
            name: "Patient X",
            xpos: 30,
            width: 300
        },
        {
            sid: 1,
            name: "Patient Y",
            xpos: 340,
            width: 100
        }
    ];

    var factory = {};
    factory.getScans = function() {
        return scans;
    }

    factory.postScan = function(scan) {
        if (scan.sid == -1) {  // Generate id for the scan
            scan.sid = scans.length;
        }
        //console.log(scan);
        scans.push(scan);
    }

    factory.updateScan = function(id, name, xpos, width) {
        for (var i = 0; i < scans.length; i++) {
            if (scans[i].sid == id) {
                scans[i].name = name;
                scans[i].xpos = xpos;
                scans[i].width = width;
            }
        }
    }
    return factory;

})

function timelineController ($scope, scanFactory){
    $scope.scans = scanFactory.getScans();
    $scope.type ="Add";
    $scope.xPosition;
    $scope.minTopixcels = 2;//1mins = 2pixcels
    $scope.updateAddScan = function(id, name, xPos, width) {
    	xPos = $scope.getXposinPixcels(xPos);
    	width= $scope.getWidthtoPixcel(width);
        if ($scope.findScan(id) !== false) {
            scanFactory.updateScan(id, name, xPos, width);
        } else {
            scanFactory.postScan({
                sid: $scope.scans.length,
                name: name,
                xpos: xPos,
                width: width
            });
        }
        $scope.scans = scanFactory.getScans();
        $scope.$apply();
    };

    $scope.findScan = function(id) {
        for (var i = 0; i < $scope.scans.length; i++) {
            if ($scope.scans[i].sid === id) {
                return $scope.scans[i];
            }
        }
        return false;
    }
    $scope.loadEditValues = function(sid, name, xpos, width) {
        scanXpos = $scope.getpixceltoMins(xpos);
        width = $scope.getPixceltoWidth(width);
        $scope.scanId = sid;
        $scope.scanName = name;
        $scope.scanXpos = scanXpos;
        $scope.scanWidth = width;
        $scope.type ="Edit";
    }
    
    $scope.loadResetValues = function() {
        $scope.scanId = "";
        $scope.scanName = "";
        $scope.scanXpos = "";
        $scope.scanWidth = "";
        $scope.type ="Add";
    }
    
    $scope.getXposinPixcels = function(xposition){
    	var currentTime = new Date();
    	var myyear=currentTime.getFullYear();
		var mymonth=currentTime.getMonth()+1;
		var mytoday=currentTime.getDate();
		var startTimeDate = mytoday+"/"+mymonth+"/"+myyear+" 00:00:00";
		
		var scanStartTime = mytoday+"/"+mymonth+"/"+myyear+" "+xposition;
		date1 = new Date(startTimeDate);
        date2 = new Date(scanStartTime);
        secs = (date2.getTime() / 1000.0) - (date1.getTime() / 1000.0);
        mins = parseInt(secs / 60);
       return (mins*$scope.minTopixcels);
    }
    
   $scope.getpixceltoMins = function(xposition){
   	var mins = xposition/($scope.minTopixcels);
   
    hour = parseInt(mins/60);
   
    mins = mins-(hour*60);
    hour = ( hour < 10 ? "0" : "" ) + hour;
    mins = ( mins < 10 ? "0" : "" ) + mins;
    return (hour+":"+mins);
   } 
   
   $scope.getWidthtoPixcel = function  (width) {
     return (width*$scope.minTopixcels);
   }
   
   $scope.getPixceltoWidth = function function_name (width) {
     return (width/$scope.minTopixcels);
   }

};


timeline.directive('addScan',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            scope.updateAddScan(scope.scanId, scope.scanName, scope.scanXpos, scope.scanWidth);
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


timeline.directive('resetScan',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            scope.loadResetValues();
            scope.$apply();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

timeline.directive('loadScanDetails',function () {

    var linker = function (scope, element, attrs) {
        element.click(function () {
        	var scan = scope.findScan(parseInt(attrs.loadScanDetails));
            scope.loadEditValues(scan.sid, scan.name, scan.xpos, scan.width);
            scope.$apply();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});