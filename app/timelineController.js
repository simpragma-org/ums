/* **** Configuration variables (global) *** */
var timelineHeight = 122; /*135;*/
var secondsPerPixel = 10;
var markingIntervalSeconds = 1800; // Show a marker every x seconds
var timeline = {};
var timelineMinScanWidth = 30 * 60;
timeline.timelineState = {
    'HIDDEN': 0,
    'VISIBLE': 1
};

/* **** End configuration variables *** */

ums.controller('timelineController', ['$scope', 'scanFactory', 'timelineService',
    function ($scope, scanFactory, timelineService) {

        $scope.ticks = [];
        $scope.dontHide = false;
        $scope.timelineHeight = timelineHeight;
        $scope.currentLeft = 0;
        $scope.scrollPixels = 40;
        $scope.maxWidth = 0;
        $scope.timelineState = timeline.timelineState.HIDDEN;
        $scope.ignoreNextClick = false; // Used when dragging cards on timeline
        // Ignore the mouseup event otherwise it is treated as a click event and
        // opens the add patient dialogue. Note that this is done based on where
        // the mouse is released not on where the box is dropped.

        $scope.minWidth = timelineMinScanWidth;
        $scope.secondsPerPixel = secondsPerPixel;

        $scope.addNewPatientTime = 1000; // Hold the mouse down for this many seconds to add a new patient


        $scope.init = function () {
            for (var i = 0; i < 48; i++) {
                var tick = {};
                if (i % 2 === 0) {
                    tick.marking = $scope.secondsToTime(i * markingIntervalSeconds);
                    tick.class = 'marking';
                } else {
                    tick.marking = false;
                    tick.class = 'marking';
                }
                tick.pos = Math.floor((i * markingIntervalSeconds) / secondsPerPixel);
                $scope.ticks.push(tick);
            }

            $scope.getScans();
            $scope.maxWidth = 86400 / secondsPerPixel;
            $scope.tick();
        };

        $scope.getScans = function () {
            $scope.scans = scanFactory.getScans();			
        };

        $scope.secondsToTime = function (seconds) {
            // In: Seconds, 0 >= seconds <  86400
            // Out: The hour corresponding to that many seconds, as HH:MM
            var mins = seconds / 60;
            var hours = Math.floor(mins / 60); // Note: Integer division does not work as expected. The Math.floor() trick will not work with negative numbers.
            var minutes = Math.floor(mins % 60); // Note that modulo works with non-integers - 3.5 % 2 = 1.5 - so we need floor here too
            return ((hours < 10) ? '0' + hours : hours) + ":" + ((minutes < 10) ? '0' + minutes : minutes);
        };

        $scope.pixelsToSeconds = function (pixels) {
            // Given a number of pixels from the origin of the timeline,
            // return the time in seconds it represents.

            return pixels * secondsPerPixel;
        };

        $scope.secondsToPixels = function (seconds) {
            return seconds / secondsPerPixel;
        };

        $scope.secondsToDate = function (seconds) {
            // Given the number of seconds since midnight,
            // return a JavaScript Date object representing the time
            var dt = new Date();
            return new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), 0, 0, seconds);
        };

        $scope.scrollWest = function (pixels) {
            // Following only works if browser is fullscreen
            if ($scope.maxWidth - Math.abs($scope.currentLeft) - pixels < window.screen.width) {
                pixels = $scope.maxWidth - (Math.abs($scope.currentLeft) + window.screen.width);
            }
            $scope.currentLeft -= pixels;
            $('#timeline').attr('style', 'transform: translate3d('+ $scope.currentLeft+'px, 0,0); transition-duration: 200ms; -webkit-transform: translate3d('+ $scope.currentLeft+'px, 0,0); transition-duration: 200ms;');
        };

        $scope.scrollEast = function (pixels) {
            if ($scope.currentLeft + pixels > 0) {
                pixels = Math.abs($scope.currentLeft);
            }
            ;
            $scope.currentLeft += pixels;
            $('#timeline').attr('style', 'transform: translate3d('+ $scope.currentLeft+'px, 0,0); transition-duration: 200ms; -webkit-transform: translate3d('+ $scope.currentLeft+'px, 0,0); transition-duration: 200ms;');
        };

        $scope.tick = function () {

			var timelinePos =  timelineService.dateToPixels(new Date());
			var applyNeeded = false;
			timelineService.currentTime(timelinePos * secondsPerPixel);

            $('#currentTimeIndicator').attr('style', 'left: ' +
                timelinePos + 'px !important;');

            setTimeout($scope.tick, secondsPerPixel * 1000);
			
			var scans = $scope.scans.slice(0);
			
			// OnTick:
			// Check if over box
			for (var i = 0; i < scans.length; i++) {
				
				// If this scan is yet to be moved to the scanner, yet is below the timeline
				if (scans[i].acquisitionStatus === acquisitionStatus.NOTONSCANNER) {
				
					if (scans[i].xpos <= timelinePos+1) {
						// Move xpos
						scans[i].xpos = timelinePos + 1;
						// move start time
						scans[i].startTime = $scope.pixelsToSeconds(scans[i].xpos);
						
						// save
						scanFactory.postScan(scans[i]);					
						applyNeeded = true;						
					}
					
				}
				
				// Is this a currently executing scan?
				if (scans[i].acquisitionStatus !== acquisitionStatus.NOTONSCANNER &&
						scans[i].acquisitionStatus !== acquisitionStatus.FINISHED) {	

					// Move it below the current time line if it is in front
					if (scans[i].startTime / secondsPerPixel > timelinePos) {

						scans[i].xpos = timelinePos;
						scans[i].startTime = $scope.pixelsToSeconds(scans[i].xpos);
						scanFactory.postScan(scans[i]);
						applyNeeded = true;					
					}
					
					// Stretch its width till it is moved off the scanner.
					if (timelinePos > scans[i].xpos + scans[i].width) {
						console.log('adjust!', scans[i].name);
						console.log(scans[i].width);
						// Increase duration, increase width
						scans[i].width += 1;
						scans[i].duration = $scope.pixelsToSeconds(scans[i].width);
						scanFactory.postScan(scans[i]);
						applyNeeded = true;
					}
				}
				
				// If the scan is finished, but it is still below the current time line, move it behind the line
				if (scans[i].acquisitionStatus === acquisitionStatus.FINISHED) {					
					if (timelinePos < scans[i].xpos + scans[i].width) {

                        // Cut off width
						//scans[i].width = timelinePos - scans[i].xpos -1;
						//scans[i].duration = $scope.pixelsToSeconds(scans[i].width);

                        // Move scan, potentially overlapping other scans.
                        scans[i].xpos -= (scans[i].xpos + scans[i].width) - timelinePos;

						scanFactory.postScan(scans[i]);
						applyNeeded = true;						
					}			
				}
			}
			
			if (applyNeeded) {
				timelineService.announceReloadTimeline();
				if(!$scope.$$phase) {
					$scope.$apply();
				}
			}
        };
        $scope.centerTimeline = function () {
            var center;
            if ($scope.currentLeft <= 0) {
                center = -$scope.currentLeft + (window.screen.width / 2); // Only works if browser is fullscreen
            }
            else {
                center = (window.screen.width / 2) - $scope.currentLeft;
            }
            var curr_time = timelineService.dateToPixels(new Date());
            if (($scope.maxWidth - curr_time) >= (window.screen.width / 2)) {

                if (center > curr_time) {
                    $scope.scrollEast(center - curr_time);
                }
                else {
                    $scope.scrollWest(curr_time - center);
                }
            }
            else {
                $scope.scrollWest($scope.maxWidth - ($scope.currentLeft + window.screen.width));
            }
        };
        $scope.recalculateTime = function (sid, xpos, seconds) {
            // Given a scan id and...
            // a new xpos: calculate start time and save
            // a new seconds: calculate start xpos and save
            // both: calculate a new start time from xpos and save
            // neither: return false

            if (typeof sid === 'undefined') throw ("recalculateTime: No sid given.");
            var scan = scanFactory.getScanById(sid);

            if (scan === null) {
                throw ("recalculateTime: No scan with supplied sid exists.");
            }

            if (xpos !== null) {
                scan.startTime = $scope.pixelsToSeconds(xpos);
                scan.originalStartTime = scan.startTime;
            } else if (time !== null) {
                scan.xpos = $scope.secondsToPixels(seconds);
            }
            scanFactory.postScan(scan);
        };

        $scope.createScan = function (scan) {
            return scanFactory.putScan(scan);
        }
        
        $scope.addPatient = function (event) {
               // Check if this event was generated by the user releasing the
                // mosue button after completing a drag event
                
                if (timelineService.ignoreNextClick === true) {
                    timelineService.ignoreNextClick = false;
                    return;
                }

                // Check if click over a box
                for (var i = 0; i < $scope.scans.length; i++) {
                    if (event.clientX > $scope.currentLeft + $scope.scans[i].xpos &&
                        event.clientX < $scope.currentLeft + $scope.scans[i].xpos + $scope.scans[i].width) {

                        // Click was over a box
						
						// Was the click over the scan in progress?
						if ($scope.scans[i].acquisitionStatus === acquisitionStatus['RUNNING'] ||
								$scope.scans[i].acquisitionStatus === acquisitionStatus['SCANCOMPLETE'] ||
									$scope.scans[i].acquisitionStatus === acquisitionStatus['PENDING']) {
									
							timelineService.announceAcquirePatient($scope.scans[i].sid);
							timelineService.announceHideTimeline();
                            return; // Click occurred over a box; do not process it further
						}

                        // Was the click on a box to the right of the current time line or to the left of it?
                        if (event.clientX < $scope.currentLeft + timelineService.dateToPixels(new Date())) {
                            timelineService.announceReviewPatient($scope.scans[i].sid);
                            timelineService.announceHideTimeline();
                            return; // Click occurred over a box; do not process it further
                        } else {
                            timelineService.announceEditPatient($scope.scans[i].sid);
                             timelineService.announceHideTimeline();
                            return; // Click occurred over a box; do not process it
                        }
                    }
                }

                // Is the click after the current time?
                if (event.clientX < $scope.currentLeft + timelineService.dateToPixels(new Date())) {
                    // If it is before the current time ignore it
                    return;
                }

                // Find previous and next box
                var prev = null;
                var next = null;
                for (i = 0; i < $scope.scans.length; i++) {

                    // Find the next scan, if any
                    if ($scope.scans[i].xpos + $scope.currentLeft > event.clientX) {
                        if ($scope.scans[i].xpos < next) {
                            next = $scope.scans[i].xpos;
                        }
                    }
                    // Find the previous scan, if any.
                    if ($scope.scans[i].xpos + $scope.scans[i].width < event.clientX) {
                        if ($scope.scans[i].xpos + $scope.scans[i].width > prev) {
                            prev = $scope.scans[i].xpos + $scope.scans[i].width;
                        }
                    }
                }

                // Do we have sufficient time to add new card?
                if (prev && next) {
                    if ((next - prev) * secondsPerPixel < minScanTime) {
                        console.log('hiding, insufficient time.');
                        return;
                    }
                }

          /*      if (new Date() - mousedownTime > 1000 &&
                    startEvent.screenX < $event.screenX * 1.03 &&
                        startEvent.screenX > $event.screenX - $event.screenX * 0.03 &&
                            startEvent.screenY < $event.screenY * 1.03 &&
                                startEvent.screenY > $event.screenY - $event.screenY * 0.03) {   */

                     
                    // Transition to the add patient screen
                    // Add a new scan
                    var xpos = event.clientX + (-$scope.currentLeft);
                    var id = $scope.createScan({
                        xpos: xpos,
                        startTime: xpos * secondsPerPixel,
                        originalStartTime: xpos * secondsPerPixel,
                        width: $scope.minWidth/secondsPerPixel,
                        duration: $scope.minWidth,
                        hospitalName: hospitalName,
                        acquisitionStatus: acquisitionStatus.NOTONSCANNER,
                        examCard: null
                    });
                    timelineService.announceAddPatient(id);
                    $scope.$apply();
                    //timelineService.announceHideTimeline();
                    //scope.scans = timelineService.getScans();

           /*     } else {
                    alert('nope')
                }  */

                // Set its starting time
                // Done
        }
        
        $scope.init();

    }
]);

ums.directive('timeline', ['timelineService',
    function (timelineService) {
        return {
            restrict: 'E',
            transclude: 'element',
            replace: true,
            template: "<div id=\"timeline-container\" show-hide-timeline init-timeline-on-load handle-clicks style=\"top: -135px\" class=\"timeline-slideUp\">" +
                "<div id=\"timescale-bg\"></div><div class=\"timeline-scroller timeline-scroller-west\" timeline-scroll=\"east\"><i class=\"icon-chevron-left icon-white\"></i></div>" +
                "<div class=\"timeline-scroller timeline-scroller-east\" timeline-scroll=\"west\"><i class=\"icon-chevron-right icon-white\"></i></div>" +
                "<div id=\"timeline\" style=\"\" ng-transclude></div></div>"
        };
    }
]);
ums.directive('timescale', ['timelineService',
    function (timelineService) {
        return {
            restrict: 'E',
            replace: true,
            template: "<ul id=\"timescale\"><li ng-repeat=\"tick in ticks\" style=\"left: {{ tick.pos }}px\" class=\"{{ tick.class }}\"><div ng-show=\"tick.marking\">{{tick.marking}}</div></li></ul>"
        };
    }
]);
ums.directive('currenttimeindicator', ['timelineService',
    function (timelineService) {
        return {
            restrict: 'E',
            replace: true,
            template: "<div id=\"currentTimeIndicator\"></div>"
        }
    }
]);
ums.directive('timelineScroll', ['timelineService',
    function (timelineService) {
        var linker = function (scope, element, attrs) {
            // Check direction
            // Scroll by an appropriate number of pixels
            element.click(function (e) {
                if (attrs.timelineScroll === 'west') {
                    scope.scrollWest(scope.scrollPixels);
                }
                else {
                    scope.scrollEast(scope.scrollPixels);
                }
                $('#timeline').attr('style', 'left: ' + scope.currentLeft + 'px !important');
                e.stopPropagation();
                e.preventDefault();
            });
        };

        return {
            restrict: 'A',
            link: linker
        }
    }
]);
ums.directive('showHideTimeline', ['timelineService',
    function (timelineService) {

        var linker = function (scope, element, attrs) {
            scope.$on('show-timeline', function () {
                if (scope.timelineState === timeline.timelineState.VISIBLE) return;
                /* DO this first to prevent scroll effects playing after the timeline has opened up. */
                scope.centerTimeline();
                scope.tick();
                /* End scroll effect prevention */
                $('#timeline-container').removeClass('timeline-slideUp hide')
                    .addClass('timeline-slideDown');
                scope.timelineState = timeline.timelineState.VISIBLE;
            });

            scope.$on('hide-timeline', function () {
                if (scope.timelineState === timeline.timelineState.HIDDEN) return;
                $('#timeline-container').removeClass('timeline-slideDown hide')
                    .addClass('timeline-slideUp');
                scope.timelineState = timeline.timelineState.HIDDEN;
            });

            scope.$on('reload-timeline', function () {
                scope.getScans();
            });
        };

        return {
            restrict: 'A',
            link: linker
        };
    }
]);

ums.directive('initTimelineOnLoad', ['timelineService',
    function (timelineService) {

        var linker = function (scope, element, attrs) {
            element.attr('style', 'height: ' + attrs.height + 'px;');
        };

        return {
            restrict: 'A',
            link: linker
        };
    }
]);

ums.directive('handleClicks', ['timelineService',
    function (timelineService) {

        var mousedownTime = 0;
        var startEvent = null;
        var timeout;
        var linker = function (scope, element, attrs) {

            element.click(function ($event) {
                // Check if this event was generated by the user releasing the
                // mosue button after completing a drag event
                if (timelineService.ignoreNextClick === true) {
                    timelineService.ignoreNextClick = false;
                    return;
                }

                // Check if click over a box
                for (var i = 0; i < scope.scans.length; i++) {
                    if ($event.clientX > scope.currentLeft + scope.scans[i].xpos &&
                        $event.clientX < scope.currentLeft + scope.scans[i].xpos + scope.scans[i].width) {

                        // Click was over a box
						
						// Was the click over the scan in progress?
						if (scope.scans[i].acquisitionStatus === acquisitionStatus['RUNNING'] ||
								scope.scans[i].acquisitionStatus === acquisitionStatus['SCANCOMPLETE'] ||
									scope.scans[i].acquisitionStatus === acquisitionStatus['PENDING']) {
									
							timelineService.announceAcquirePatient(scope.scans[i].sid);
							timelineService.announceHideTimeline();
                            return; // Click occurred over a box; do not process it further
						}

                        // Was the click on a box to the right of the current time line or to the left of it?
                        if ($event.clientX < scope.currentLeft + timelineService.dateToPixels(new Date())) {
                            timelineService.announceReviewPatient(scope.scans[i].sid);
                            timelineService.announceHideTimeline();
                            return; // Click occurred over a box; do not process it further
                        } else {
                            timelineService.announceEditPatient(scope.scans[i].sid);
                             timelineService.announceHideTimeline();
                            return; // Click occurred over a box; do not process it
                        }
                    }
                }

                // Is the click after the current time?
                if ($event.clientX < scope.currentLeft + timelineService.dateToPixels(new Date())) {
                    // If it is before the current time ignore it
                    return;
                }

                // Find previous and next box
                var prev = null;
                var next = null;
                for (i = 0; i < scope.scans.length; i++) {

                    // Find the next scan, if any
                    if (scope.scans[i].xpos + scope.currentLeft > $event.clientX) {
                        if (scope.scans[i].xpos < next) {
                            next = scope.scans[i].xpos;
                        }
                    }
                    // Find the previous scan, if any.
                    if (scope.scans[i].xpos + scope.scans[i].width < $event.clientX) {
                        if (scope.scans[i].xpos + scope.scans[i].width > prev) {
                            prev = scope.scans[i].xpos + scope.scans[i].width;
                        }
                    }
                }
/*
                // Do we have sufficient time to add new card?
                if (prev && next) {
                    if ((next - prev) * secondsPerPixel < minScanTime) {
                        console.log('hiding, insufficient time.');
                        return;
                    }
                }


                if (new Date() - mousedownTime > 1000 &&
                    startEvent.pageX < $event.pageX * 1.03 &&
                        startEvent.pageX > $event.pageX - $event.pageX * 0.03 &&
                            startEvent.pageY < $event.pageY * 1.03 &&
                                startEvent.pageY > $event.pageY - $event.pageY * 0.03) {



                    // Transition to the add patient screen
                    // Add a new scan
                    var xpos = $event.clientX + (-scope.currentLeft);
                    var id = scope.createScan({
                        xpos: xpos,
                        startTime: xpos * secondsPerPixel,
                        originalStartTime: xpos * secondsPerPixel,
                        width: scope.minWidth/secondsPerPixel,
                        duration: scope.minWidth,
                        hospitalName: hospitalName,
                        acquisitionStatus: acquisitionStatus.NOTONSCANNER,
                        examCard: null
                    });
                    timelineService.announceAddPatient(id);
                    scope.$apply();
                    timelineService.announceHideTimeline();
                    //scope.scans = timelineService.getScans();

                } else {


                }*/


                // Set its starting time
                // Done
            });
            
            element.bind('mousedown',function($event) {
               var event = $event;
               timeout = setTimeout(function(){
                   
                   scope.addPatient(event);
                   
               }, scope.addNewPatientTime);
            });

            
             element.bind('mouseup',function($event) {
                clearTimeout(timeout);
            });
            
            element.bind('touchstart',function($event) {
               element.unbind('mousedown');
               element.unbind('mouseup');
               var event = $event.originalEvent.touches[0];
               timeout = setTimeout(function(){
                   
                   scope.addPatient(event);
                   
               }, 3000); 
            });
            
            element.bind('touchend',function($event) {
                clearTimeout(timeout);
            });
            


        };

        return {
            restrict: 'A',
            link: linker
        };
    }
]);

ums.directive('draggable', ['timelineService',
    function (timelineService) {

        return function (scope, element, attr) {
            var startX = 0, x = 0;
            var scanIndex = -1;
            var currScanRight = -1;
            var originalXpos = 0;
            var dragged = false;
            var pos = 0;
            var init_xpos = 0;
            var flag = 0;

            element.bind('mousedown', function (event) {

                event.preventDefault();
                startX = event.clientX - x;

                // Find the entry in scope.scans corresponding to what was clicked on
                for (var i = 0; i < scope.scans.length; i++) {
                    if (scope.scans[i].sid == attr.loadScanDetails) {
                        scanIndex = i;
                        break;
                    }
                }

                // Find the scan in progress
                for (var i = 0; i < scope.scans.length; i++) {
                    if (scope.scans[i].xpos <= timelineService.dateToPixels(new Date())) {
                        if ((scope.scans[i].xpos + scope.scans[i].width) >= timelineService.dateToPixels(new Date())) {
                            currScanRight = scope.scans[i].xpos + scope.scans[i].width;
                            break;
                        }
                    }
                }


                originalXpos = scope.scans[scanIndex].xpos;

                if (scope.scans[scanIndex].xpos > timelineService.dateToPixels(new Date())) {
                    element.bind('mousemove', mousemove);
                    element.bind('mouseup', mouseup);
                    element.bind('mouseout', mouseout);
                }

                if (flag == 0) {
                    init_pos = scope.scans[scanIndex].xpos;
                    flag++;
                }

                dragged = false;

            });

            function mouseout(event) {
                element.unbind('mousemove', mousemove);
                element.unbind('mouseup', mouseup);
                dragged = false;
            }

            function mousemove(event) {

                x = (event.clientX - scope.currentLeft) - (startX - scope.currentLeft);
                pos = x - pos;
                if (currScanRight != -1) {

                    if (scope.scans[scanIndex].xpos + pos > currScanRight) {
                        scope.scans[scanIndex].xpos += pos;
                        scope.$apply();
                    }
                    else {
                        x = scope.scans[scanIndex].xpos - init_pos;
                    }
                    //console.log(currScanRight, scope.scans[scanIndex].xpos, x);
                }

                //if there is no scan in progress
                else {

                   // console.log(x + init_pos, scope.scans[scanIndex].xpos, event.clientX)
                    if (scope.scans[scanIndex].xpos + pos > timelineService.dateToPixels(new Date())) {
                        scope.scans[scanIndex].xpos += pos;
                        scope.$apply();
                    }
                    else {
                        x = scope.scans[scanIndex].xpos - init_pos;
                    }
                }
                scope.recalculateTime(scope.scans[scanIndex].sid, scope.scans[scanIndex].xpos);
                dragged = true;
                pos = x;
                target = event.target;
            }

            function mouseup(event) {
                if (dragged) {
                    timelineService.ignoreNextClick = true;
                    //timelineService.announceReloadScans();
                    timelineService.announceTimeChanged();
                }
                element.unbind('mousemove', mousemove);
                element.unbind('mouseup', mouseup);
                scope.getScans();
				scope.$apply();
            }
        }
    }
]);


ums.directive('scrollable', ['timelineService',
    function (timelineService) {

        var lastTouchmoveDistance = null;
        var lastTouchmoveDirection = null;
        var dragged = false;

        return function (scope, element, attr) {
            element.bind('mousewheel', function (event) {

                var delta = 0;

                if (!event) event = window.event;
                if (event.originalEvent.wheelDelta) {
                    delta = event.originalEvent.wheelDelta / 120;
                    if (window.opera) delta = -delta;
                } else if (event.originalEvent.detail) {
                    delta = -event.originalEvent.detail / 3;
                }
                if (delta) {
                    if (delta < 0) {
                        scope.scrollWest(scope.scrollPixels);
                    }
                    else {
                        scope.scrollEast(scope.scrollPixels);
                    }
                }

                /** Prevent default actions caused by mouse wheel.
                 * That might be ugly, but we handle scrolls somehow
                 * anyway, so don't bother here..
                 */
                if (event.preventDefault)
                    event.preventDefault();
                event.returnValue = false;

                scope.$apply();
        });
        element.hammer({ drag_lock_to_axis: true })
            .on("release dragleft dragright swipeleft swiperight swipedown swipeup dragend", function($event) {

                var eventType = null;
                if ($event.type == 'release') {
                    eventType = $event.gesture.direction;
                } else {
                    eventType = $event.type;
                }
                if (eventType == 'dragleft' || eventType == 'dragright') {
                    if ($event.gesture.distance == lastTouchmoveDistance) {
                        //$event.preventDefault();
                        //$event.stopPropagation();
                        //return false;
                    }
                    var delta = 0;
                    if ($event.gesture.direction !== lastTouchmoveDirection) {
                        delta = $event.gesture.distance;
                    } else {
                        delta = $event.gesture.distance - lastTouchmoveDistance;
                    }
                    lastTouchmoveDirection = $event.gesture.direction;
                    lastTouchmoveDistance = $event.gesture.distance;
                }
                if (eventType == 'swiperight' || eventType == 'eventleft') {
                    if (dragged) {
                        //$event.preventDefault();
                       // $event.stopPropagation();
                        //return false;
                    }
                }
                switch (eventType) {
                    case 'swipeleft':
                        scope.scrollWest($event.gesture.distance);
                        break;

                    case 'swiperight':
                        scope.scrollEast($event.gesture.distance);
                        break;

                    case 'dragleft':
                        scope.scrollWest(delta);
                        dragged = true;
                        break;

                    case 'dragright':
                        scope.scrollEast(delta);
                        dragged = true;
                        break;
                    case 'dragend':
                        lastTouchmoveDirection = null;
                        lastTouchmoveDistance = 0;
                        dragged = false;
                        break;
                }
                scope.$apply();
                //$event.preventDefault();
                //$event.stopPropagation();
                return false;
            });



        }
    }

]);
