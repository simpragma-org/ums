/* Main application controller. Handles events and transitions */
/* Note: This gets all mousemove events, then passes them to other
   controllers if needed. Other controllers will receive mousemove
   events only from this.
 */



/* **** Global configuration variables *** */
/* ToDo: Move into separate object to lessen global namespace pollution */


var clickerRegionExtent = 5; // Show clicker if mouse within this many px from each edge of screen
var stackRegionExtent = 40; // Show stack if in a stackable card and cursor at this position
var minStackHotspotTime = 500;
var timelineHotspotExtent = timelineHeight; // Show timeline if mouse in this region
var minTimelineHotspotTime = 1750; // Show only if cursor in hotspot for this long

var clickers = {
    "NORTH": "#clicker-north",
    "SOUTH": "#clicker-south",
    "EAST": "#clicker-east",
    "WEST": "#clicker-west"
};
var directions = {
    "EAST": 0,
    "WEST": 1,
    "NORTH": 2,
    "SOUTH": 3
}
var cards = {
    "NORTH": "#north",
    "NORTHEAST": "#northeast",
    "NORTHWEST": "#northwest",
    "SOUTH": "#south",
    "SOUTHWEST": "#southwest",
    "SOUTHEAST": "#southeast",
    "SOUTHEAST1": "#southeast1",
    "SOUTHEAST2": "#southeast2",
    "SOUTHEAST3": "#southeast3",
    "SOUTHEAST4": "#southeast4",
    "SOUTHEAST5": "#southeast5",
    "SOUTHEAST6": "#southeast6",
    "SOUTHEAST7": "#southeast7",
    "southarray":{
    "val":"#value"
    }
    
}



var stacks = {
    "NORTHEAST": 'northeast-slideWest',
    "NORTHWEST": 'northwest-slideEast',
    "SOUTH": 'south-slideWest'
}
/* **** /Global configuration variables *** */

ums.controller('mainController', ['$scope', 'privilegesFactory','featuresFactory','rolesFactory','usersFactory','userroleFactory','roleprivilegefeatureFactory','departmentsFactory',
    function ($scope, privilegesFactory,featuresFactory,rolesFactory,usersFactory,userroleFactory,roleprivilegefeatureFactory,departmentsFactory) {


    $scope.visibleClicker = false;
    $scope.currentCard = cards.NORTH;
    $scope.visibleStack = null;
    $scope.lastStackHotspotEventTime = null;
    $scope.timelineVisible = false;
    $scope.privileges = privilegesFactory.getPrivileges();
	$scope.features = featuresFactory.getFeatures();
	$scope.roles = rolesFactory.getRoles();
	$scope.users = usersFactory.getUsers();
	$scope.departments = departmentsFactory.getDepartments();
	//console.log($scope.departments);
	$scope.userrole = userroleFactory.getUsersWithRoles();
	$scope.roleprivilegefeatures = roleprivilegefeatureFactory.getRolesPrivilegesFeatures();
	//$scope.color = 'red';
	//console.log($scope.color);
	$scope.color = 'blue';
    
    $scope.alert = function() {
        alert($scope.color);        
    }
	//console.log($scope.features);
	//console.log($scope.users);
	//console.log($scope.userrole);
	//console.log($scope.roleprivilegefeatures);
	//$scope.valueprivilege =1;
	
	$scope.newValue = function(value) {
    //alert($scope.valueprivilege);
     $scope.editPrivilegeId = $scope.valueprivilege;
    // console.log($scope.editPrivilegeId);
      $scope.privilegeEdit = privilegesFactory.getPrivilegeById($scope.editPrivilegeId);	
   	
}

$scope.newValueFeature = function(value) {
     
     $scope.editFeatureId = $scope.valuefeature;
     console.log('value='+value);
      $scope.featureEdit = featuresFactory.getFeatureById($scope.editFeatureId);
     // console.log($scope.featureEdit);	
   	
}

$scope.newValueRole = function(value) {
     
     $scope.editRoleId = $scope.valuerole;
    // console.log('value='+value);
      $scope.roleEdit = rolesFactory.getRoleById($scope.editRoleId);
     // console.log($scope.featureEdit);	
   	
}

$scope.newValueUser = function(value) {
     
     $scope.editUserId = $scope.valueuser;
     
      $scope.userEdit = usersFactory.getUserById($scope.editUserId);
     // console.log('value='+$scope.userEdit.department);
      $scope.userEdit.department = departmentsFactory.getDepartmentById($scope.userEdit.department);
      //console.log($scope.userEdit.uid);
   $scope.userEdit.supervisor =  usersFactory.getUserById($scope.userEdit.supervisor);	
   $scope.userEdit.Role = userroleFactory.getUserroleById($scope.userEdit.uid);
   console.log($scope.userEdit.Role);
}

$scope.newValueroleprivilegeFeature = function(value) {
     
     $scope.editPfrid = $scope.valueroleprivilegeFeature;
     
      
   console.log($scope.editPfrid);
}


	$scope.updateAddPrivilege = function(id, title) {
    	//console.log($scope.privilegePid);
    	for(i=0;i<$scope.privileges.length;i++){
    		
    		var pid = $scope.privileges[i].pid;
    	}
    	pid++;
    	console.log(pid);
        if ($scope.findScan(id) !== false) {
            privilegesFactory.updatePrivilege(id, title);
        } else {
            privilegesFactory.postPrivilege({
                pid: pid,
                title: title,
               
            });
        }
        $scope.privileges = privilegesFactory.getPrivileges();
        $scope.$apply();
        
    }
    
    $scope.updateAddFeature = function(id, title) {
    	//console.log(id);
    	for(i=0;i<$scope.features.length;i++){
    		
    		var fid = $scope.features[i].fid;
    	}
    	fid++;
    	
        if ($scope.findFeature(id) !== false) {
        	//console.log('feature');
            privilegesFactory.updatePrivilege(id, title);
        } else {
            featuresFactory.postFeature({
                pid: fid,
                title: title,
               
            });
        }
        $scope.features = featuresFactory.getFeatures();
        $scope.$apply();
        
    }
    
    $scope.updateAddRole = function(id, title) {
    	//console.log(id);
    	for(i=0;i<$scope.roles.length;i++){
    		
    		var rid = $scope.roles[i].rid;
    	}
    	rid++;
    	
        if ($scope.findRole(id) !== false) {
        	//console.log('feature');
            privilegesFactory.updatePrivilege(id, title);
        } else {
            rolesFactory.postRole({
                rid: rid,
                title: title,
               
            });
        }
        $scope.roles = rolesFactory.getRoles();
        $scope.$apply();
        
    }
    
    $scope.updateAddUser = function(uid,Name,Address,Email_id,Phone_number,Password,department,supervisor,Role) {
    	//console.log(id);
    	
    	for(i=0;i<$scope.users.length;i++){
    		
    		var uid = $scope.users[i].uid;
    	}
    	uid++;
    	
        if ($scope.findUser(uid) !== false) {
        	//console.log('feature');
            privilegesFactory.updatePrivilege(id, title);
        } else {
            usersFactory.postUser({
                uid: uid,
                Name: Name,
                Address:Address,
                Email_id:Email_id,
                Phone_number:Phone_number,
                Password:Password,
                department:[department.did],
                supervisor:[supervisor.uid]
               
            });
            
            userroleFactory.postUserrole({
                uid: [uid],
                rid: [Role.rid]
               
            });
        }
        $scope.users = usersFactory.getUsers();
        $scope.userrole = userroleFactory.getUsersWithRoles();
        $scope.$apply();
        console.log($scope.userrole);
        
    }

	
    $scope.updateAddRolePrivilegeFeature = function(Role,Feature,Privilege) {
    
    	for(i=0;i<$scope.roleprivilegefeatures.length;i++){
    		
    		var pfrid = $scope.roleprivilegefeatures[i].pfrid;
    	}
    	pfrid++;
            roleprivilegefeatureFactory.postRolePrivilegeFeature({
                pfrid:pfrid,
                rid: [Role.rid],
                fid:[Feature.fid],
                pid:[Privilege.pid]
               
            });
       
        $scope.roleprivilegefeatures = roleprivilegefeatureFactory.getRolesPrivilegesFeatures();
       
        $scope.$apply();
       
    }

    
    $scope.findScan = function(id) {
        for (var i = 0; i < $scope.privileges.length; i++) {
            if ($scope.privileges[i].pid === id) {
                return $scope.privileges[i];
            }
        }
        return false;
    }
    
    $scope.findFeature = function(id) {
    	//console.log('id='+id);
        for (var i = 0; i < $scope.features.length; i++) {
            if ($scope.features[i].fid === id) {
            	//console.log('hello'+id);
                return $scope.features[i];
            }
        }
        return false;
    }
    
    
    $scope.findRole = function(id) {
    	//console.log('id='+id);
        for (var i = 0; i < $scope.roles.length; i++) {
            if ($scope.roles[i].rid === id) {
            	//console.log('hello'+id);
                return $scope.roles[i];
            }
        }
        return false;
    }
    
    $scope.findUser = function(id) {
    	//console.log('id='+id);
        for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.users[i].uid === id) {
            	//console.log('hello'+id);
                return $scope.users[i];
            }
        }
        return false;
    }
    
   $scope.deletePrivilege = function(pid){
   //	console.log(pid);
   	privilegesFactory.popPrivilege({
                pid: pid,
               
            });
        $scope.privileges = privilegesFactory.getPrivileges();
        $scope.$apply();
        //console.log( $scope.privileges);
   	
   } 
    
   $scope.deleteFeature = function(fid){
   //	console.log(pid);
   	featuresFactory.popFeature({
                fid: fid,
               
            });
        $scope.features = featuresFactory.getFeatures();
        $scope.$apply();
   	
   }
   
    $scope.deleteRole = function(rid){
   //	console.log(pid);
   	rolesFactory.popRole({
                rid: rid,
               
            });
        $scope.roles = rolesFactory.getRoles();
        $scope.$apply();
   	
   } 
   
    $scope.deleteUser = function(uid){
   //	console.log(pid);
   	usersFactory.popUser({
                uid: uid,
               
            });
        $scope.users = usersFactory.getUsers();
        $scope.$apply();
   	
   }   
 
 
 $scope.deleteRolePrivilegeFeature = function(pfrid){
   //	console.log(pid);
   	roleprivilegefeatureFactory.popRolePrivilegeFeature({
                pfrid: pfrid,
               
            });
       $scope.roleprivilegefeatures = roleprivilegefeatureFactory.getRolesPrivilegesFeatures();
       
        $scope.$apply();
   	
   } 
 
}]);



ums.directive('showUsers',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            $('#privilegeSection').hide();
            $('#featureSection').hide();
            $('#roleSection').hide();
            $('#userSection').show();
            $('#rolefeatureprivilegeSection').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('showRoles',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            $('#privilegeSection').hide();
            $('#featureSection').hide();
            $('#roleSection').show();
            $('#userSection').hide();
            $('#rolefeatureprivilegeSection').hide();
        });


    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('showFeatures',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            $('#privilegeSection').hide();
            $('#featureSection').show();
            $('#roleSection').hide();
            $('#userSection').hide();
            $('#rolefeatureprivilegeSection').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('showPrivileges',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            $('#privilegeSection').show();
            $('#featureSection').hide();
            $('#roleSection').hide();
            $('#userSection').hide();
            $('#rolefeatureprivilegeSection').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('showUsers',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            $('#privilegeSection').hide();
            $('#featureSection').hide();
            $('#roleSection').hide();
            $('#userSection').show();
            $('#rolefeatureprivilegeSection').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('showRoleFeaturePrivilege',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            $('#privilegeSection').hide();
            $('#featureSection').hide();
            $('#roleSection').hide();
            $('#userSection').hide();
            $('#rolefeatureprivilegeSection').show();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('showAddPrivilege',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
         // console.log(scope.privilegePid);
            $('#addPrivilege').show();
            $('#editPrivilege').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('showAddFeature',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
         // console.log(scope.privilegePid);
            $('#addFeature').show();
            $('#editFeature').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('showAddRole',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
         // console.log(scope.privilegePid);
            $('#addRole').show();
            $('#editRole').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('showAddUser',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
         // console.log(scope.privilegePid);
            $('#addUser').show();
            $('#editUser').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('showAddRolePrivilegeFeature',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
         // console.log(scope.privilegePid);
            $('#AddRolePrivilegeFeature').show();
            //$('#editUser').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('showEditPrivilege',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
           console.log(scope.privilegeEdit);
            $('#editPrivilege').show();
             $('#addPrivilege').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('showEditFeature',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
          // console.log(scope.privilegeEdit);
            $('#editFeature').show();
             $('#addFeature').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('showEditRole',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
          // console.log(scope.privilegeEdit);
            $('#editRole').show();
             $('#addRole').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('showEditUser',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
           console.log(scope.userEdit);
            $('#editUser').show();
             $('#addUser').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('addPrivilege',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	
            scope.updateAddPrivilege(scope.pid, scope.title);
            $('#addPrivilege').hide();
            $('#editPrivilege').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('addFeature',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	
            scope.updateAddFeature(scope.fid, scope.titleFeature);
            $('#addFeature').hide();
            $('#editFeature').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('addRole',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	
            scope.updateAddRole(scope.rid, scope.titleRole);
            $('#addRole').hide();
            $('#editRole').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('addUser',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	//console.log('hi');
        	//console.log(scope.department);
        	//console.log(scope.department.did);
        	//console.log(scope.supervisor);
            scope.updateAddUser(scope.uid, scope.Name,scope.Address,scope.Email_id,scope.Phone_number,scope.Password,scope.department,scope.supervisor,scope.Role);
           // console.log(scope.department);
            $('#addUser').hide();
            $('#editUser').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('addRolePrivilegeFeature',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	//console.log('hi');
        	//console.log(scope.department);
        	//console.log(scope.department.did);
        	//console.log(scope.supervisor);
            scope.updateAddRolePrivilegeFeature(scope.Role, scope.Feature,scope.Privilege);
           // console.log(scope.department);
            $('#AddRolePrivilegeFeature').hide();
           // $('#editUser').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('editPrivilege',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	
            scope.updateAddPrivilege(scope.privilegeEdit.pid, scope.privilegeEdit.title);
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('deletePrivilege',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	
            scope.deletePrivilege(scope.privilegeEdit.pid);
             $('#addPrivilege').hide();
            $('#editPrivilege').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('deleteFeature',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	
            scope.deleteFeature(scope.featureEdit.fid);
             $('#addFeature').hide();
            $('#editFeature').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('deleteRole',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	
            scope.deleteRole(scope.roleEdit.rid);
             $('#addRole').hide();
            $('#editRole').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});


ums.directive('deleteUser',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	console.log(scope.userEdit.uid);
            scope.deleteUser(scope.userEdit.uid);
             $('#addUser').hide();
            $('#editUser').hide();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('deleteRolePrivilegeFeature',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
        	//console.log(scope.userEdit.uid);
            scope.deleteRolePrivilegeFeature(scope.editPfrid);
             
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('transitionPage', function () {

    var linker = function (scope, element, attrs) {
        element.click(function () {

            scope.transitionPage(directions[attrs.transitionPage]);
            // Hide clicker below the cursor if we have reached the end and cannot transition
            // further in this direction.
            if (scope.getTransitionDestination(scope.currentCard, directions[attrs.transitionPage]) === false)  {
                scope.hideClickers();
            }

        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('addScan',[function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            scope.updateAddScan(scope.scanId, scope.scanName, scope.scanXpos, scope.scanWidth);
        });
    };

    return {
        restrict:'A',
        link:linker
    }
}]);


ums.directive('resetScan',function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
            scope.loadResetValues();
            scope.$apply();
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('swapWestStack',function () {
	//console.log(id);
    var linker = function (scope, element, attrs) {
			
        element.click(function () {
        	var currentId = attrs.swapWestStack;
           var currentText = $(currentId).text();
        	
        	curentTackText = $("#northwest").text();
        	
        	$(currentId).text(curentTackText);
        	$("#northwest h2").text(currentText);
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('swapEastStack',function () {
	//console.log(id);
    var linker = function (scope, element, attrs) {
			
        element.click(function () {
        	
        	var currentId = attrs.swapEastStack;
           var currentText = $(currentId).text();
        	
        	curentTackText = $("#northeast").text();
        	
        	$(currentId).text(curentTackText);
        	$("#northeast h2").text(currentText);
        });
    };

    return {
        restrict:'A',
        link:linker
    }
});

ums.directive('addUi',[function () {

    var linker = function (scope, element, attrs) {

        element.click(function () {
               alert('Are you sure to add new Screen');
               
               $hidden = $('#1').attr('value');
               $hiddenId = $('#southEastID').attr('value');
               $('#cards-perspective').append("<div id='southeast"+$hiddenId+"' class='card'><h2> Tools UI "+$hidden+" </h2></div>");
                alert("New Tool UI "+$hidden+" is Added");
               $hidden++; 
               $hiddenId++;               
               $('#1').attr('value',$hidden);
               $('#southEastID').attr('value',$hiddenId); 
               
               var countUI = $('#southEastID').attr('value');
				var southArray = '';
				for(var ii = 1;ii <countUI;ii++){
				southArray+="'SOUTHEAST'"+ii+"':#southeast'"+ii+",";
				}


              });
    };

    return {
        restrict:'A',
        link:linker
    }
}]);
