ums.factory('privilegesFactory', function() {
	 var factory = {};
    var privileges = [
        {
            pid: 1,
            title: 'Add',
           
        },
        {
            pid: 2,
            title: 'Delete',
           
        },
        {
            pid: 3,
            title: 'Modify',
           
        },
        {
            pid: 4,
            title: 'Assign',
           
        }
    ];

   
    factory.getPrivileges = function() {
        return privileges;
    }
    
     factory.getPrivilegeById=function(id) {
           for(var i = 0;i < privileges.length; i++){
               if(privileges[i].pid == id){
                   return privileges[i];
               }
           }
           return null;
        };

    factory.postPrivilege = function(privilege) {
        if (privilege.pid == -1) {  // Generate id for the scan
            privilege.pid = privileges.length;
        }
        privileges.push(privilege);
    }

    factory.updatePrivilege = function(pid, title) {
        for (var i = 0; i < privileges.length; i++) {
            if (privileges[i].pid == pid) {
                privileges[i].title = title;
                
            }
        }
    }
    
    factory.popPrivilege = function(privilege) {
       // console.log(privilege.pid);
        pid = (privilege.pid-1);
        privileges.splice(pid, 1);;
    }

    return factory;
}); 

ums.factory('featuresFactory', function() {
	 var factory = {};   
    //Features
    var features = [
        {
            fid: 1,
            title: 'Sales',
           
        },
        {
            fid: 2,
            title: 'Marketing',
           
        }
    ];

  
    factory.getFeatures = function() {
        return features;
    }

	factory.getFeatureById=function(id) {
           for(var i = 0;i < features.length; i++){
               if(features[i].fid == id){
                   return features[i];
               }
           }
           return null;
        };

    factory.postFeature = function(feature) {
        if (feature.fid == -1) {  // Generate id for the scan
            feature.fid = features.length;
        }
        features.push(feature);
    }

    factory.updateFeature = function(fid, title) {
        for (var i = 0; i < features.length; i++) {
            if (features[i].fid == fid) {
               features[i].title = title;
                
            }
        }
    }
    
    
    factory.popFeature = function(feature) {
       // console.log(privilege.pid);
        fid = (feature.fid-1);
        features.splice(fid, 1);;
    }
    
     return factory;

});

//Roles
ums.factory('rolesFactory', function() {
	 var factory = {}; 
    var roles = [
        {
            rid: 1,
            title: 'HR',
           
        },
        {
            rid: 2,
            title: 'Manager',
           
        },
         {
            rid: 3,
            title: 'Admin',
           
        }
    ];

  
    factory.getRoles = function() {
        return roles;
    }


	factory.getRoleById=function(id) {
           for(var i = 0;i < roles.length; i++){
               if(roles[i].rid == id){
                   return roles[i];
               }
           }
           return null;
        };
        
    factory.postRole = function(role) {
        if (role.rid == -1) {  // Generate id for the scan
            role.rid = roles.length;
        }
        roles.push(role);
    }

    factory.updateRole = function(rid, title) {
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].rid == rid) {
               roles[i].title = title;
                
            }
        }
    }
    
    factory.popRole = function(role) {
       // console.log(privilege.pid);
        rid = (role.rid-1);
        roles.splice(rid, 1);;
    }
     return factory;

});

//Departments
ums.factory('departmentsFactory', function() {
	 var factory = {}; 
    var departments = [
        {
            did: 1,
            title: 'HR'
           
        },
        {
            did: 2,
            title: 'Finance'
           
        },
         {
            did: 3,
            title: 'Admin'
           
        }
    ];

  
    factory.getDepartments = function() {
        return departments;
    }
    
    factory.getDepartmentById=function(id) {
           for(var i = 0;i < departments.length; i++){
               if(departments[i].did == id){
                   return departments[i];
               }
           }
           return null;
        };

    factory.postDepartment = function(department) {
        if (department.did == -1) {  // Generate id for the scan
            department.did = departments.length;
        }
        roles.push(department);
    }

    factory.updateDepartment = function(did, title) {
        for (var i = 0; i < departments.length; i++) {
            if (departments[i].did == did) {
               departments[i].title = title;
                
            }
        }
    }
     return factory;

});


//Users
ums.factory('usersFactory',['departmentsFactory',
 function(departmentsFactory) {
	 var factory = {}; 
    var users = [
        {
            uid: 1,
            Name: 'Ashwath',
            Address:'aaaaasadadad',
            photo:'',
            Email_id:'ashwath@simpragma.com',
            Phone_number:'9743284869',
            Password:'*********',
            department:[1],
            supervisor:[1]
           
        },
        {
            uid: 2,
            Name: 'Harish',
            Address:'aaaaasadadad',
            photo:'',
            Email_id:'ashwath@simpragma.com',
            Phone_number:'9743284869',
            Password:'*********',
            department:[2],
            supervisor:[1]
           
        },
         {
             uid: 3,
            Name: 'Akash',
            Address:'aaaaasadadad',
            photo:'',
            Email_id:'ashwath@simpragma.com',
            Phone_number:'9743284869',
            Password:'*********',
            department:[3],
            supervisor:[1]
           
        }
    ];

  
    factory.getUsers = function() {
        return users;
    };


	factory.getUserById=function(id) {
           for(var i = 0;i < users.length; i++){
               if(users[i].uid == id){
                   return users[i];
               }
           }
           return null;
        };
        
    factory.postUser = function(user) {
        if (user.uid == -1) {  // Generate id for the scan
            user.uid = users.length;
        }
        users.push(user);
    }

    factory.updateUser = function(uid, Name) {
        for (var i = 0; i < users.length; i++) {
            if (users[i].uid == uid) {
               users[i].Name = Name;
                
            }
        }
    }

	factory.getUsersWithSupervisors = function()
	{
	
	            var ec = users;
	            var protocols = factory.getUsers();
	            var departments = departmentsFactory.getDepartments();
	            console.log(departments);
	           // console.log(protocols);
	            // Replace each protocol id with its actual protocol object
	            for (var i = 0; i < ec.length; i++) {
	                for (var p = 0; p < ec[i].supervisor.length; p++) {
	
	                    // Find protocol with the given id in the list of protocols
	                    for (var j = 0; j < protocols.length; j++) {
	                    	//console.log('hi');
	                        if (protocols[j].uid === ec[i].supervisor[p]) {
	                            // And replace the id with the actual object
	                            ec[i].supervisor[p] = protocols[j];
	                            break;
	                        }
	                    }
	                }
	                
	                for (var p = 0; p < ec[i].department.length; p++) {
	
	                    // Find protocol with the given id in the list of protocols
	                    for (var j = 0; j < departments.length; j++) {
	                    	//console.log('hi');
	                        if (departments[j].did === ec[i].department[p]) {
	                            // And replace the id with the actual object
	                            ec[i].department[p] = departments[j];
	                            break;
	                        }
	                    }
	                }
	                
	            }
	            return ec;
		
		
	}
	
	 factory.popUser = function(user) {
       // console.log(privilege.pid);
        uid = (user.uid-1);
        users.splice(uid, 1);;
    }
	
    return factory;

}
]);



//User roles
ums.factory('userroleFactory',['usersFactory','rolesFactory',
 function(usersFactory,rolesFactory) {
	 var factory = {}; 
    var userroles = [
        {
            uid: [1],
            rid:[1]
              
        },
        { 
        	uid: [2],
            rid:[2]  
        },
         {
            uid: [3],
            rid:[3]
           
        }
    ];

  
    factory.getUserroles = function() {
        return userroles;
    };

	factory.getUserroleById=function(id) {
           for(var i = 0;i < userroles.length; i++){
           	console.log(id);
           	console.log(userroles[i].uid[0].uid);
               if(userroles[i].uid[0].uid == id){
               	
                   return userroles[i].rid[0];
               }
           }
           return null;
        };
	
    factory.postUserrole = function(userrole) {
        
        userroles.push(userrole);
    }

    factory.updateUserrole = function(uid, rid) {
        for (var i = 0; i < userroles.length; i++) {
            if (userroles[i].uid == uid) {
               userroles[i].rid = rid;
                
            }
        }
    }

	factory.getUsersWithRoles = function()
	{
	
	            var ec = userroles;
	            var users = usersFactory.getUsers();
	            var roles = rolesFactory.getRoles();
	           // console.log(departments);
	           // console.log(protocols);
	            // Replace each protocol id with its actual protocol object
	            for (var i = 0; i < ec.length; i++) {
	                for (var p = 0; p < ec[i].uid.length; p++) {
	
	                    // Find protocol with the given id in the list of protocols
	                    for (var j = 0; j < users.length; j++) {
	                    	//console.log('hi');
	                        if (users[j].uid === ec[i].uid[p]) {
	                            // And replace the id with the actual object
	                            ec[i].uid[p] = users[j];
	                            break;
	                        }
	                    }
	                }
	                
	                for (var p = 0; p < ec[i].rid.length; p++) {
	
	                    // Find protocol with the given id in the list of protocols
	                    for (var j = 0; j < roles.length; j++) {
	                    	//console.log('hi');
	                        if (roles[j].rid === ec[i].rid[p]) {
	                            // And replace the id with the actual object
	                            ec[i].rid[p] = roles[j];
	                            break;
	                        }
	                    }
	                }
	                
	            }
	            return ec;
		
		
	}
    return factory;

}
]);


//roles privileges and feature factory
ums.factory('roleprivilegefeatureFactory',['rolesFactory','privilegesFactory','featuresFactory',
 function(rolesFactory,privilegesFactory,featuresFactory) {
	 var factory = {}; 
    var rolesprivilegesfeatures = [
        {
        	pfrid:1,
            rid: [1],
            pid:[1],
            fid:[1]
              
        },
        { 
        	pfrid:2,
        	rid: [1],
            pid:[2],
            fid:[1] 
        },
         {
         	pfrid:3,
            rid: [1],
            pid:[1],
            fid:[2]
           
        }
    ];

  
  

    factory.postRolePrivilegeFeature = function(roleprivilegefeature) {
        
        rolesprivilegesfeatures.push(roleprivilegefeature);
    }

   

	factory.getRolesPrivilegesFeatures= function()
	{
	
	            var ec = rolesprivilegesfeatures;
	           
	            var roles = rolesFactory.getRoles();
	            var privileges = privilegesFactory.getPrivileges();
	            var features = featuresFactory.getFeatures();
	           // console.log(departments);
	           // console.log(protocols);
	            // Replace each protocol id with its actual protocol object
	            for (var i = 0; i < ec.length; i++) {
	                
	                for (var p = 0; p < ec[i].rid.length; p++) {
	
	                    // Find protocol with the given id in the list of protocols
	                    for (var j = 0; j < roles.length; j++) {
	                    	//console.log('hi');
	                        if (roles[j].rid === ec[i].rid[p]) {
	                            // And replace the id with the actual object
	                            ec[i].rid[p] = roles[j];
	                            break;
	                        }
	                    }
	                }
	                
	                for (var p = 0; p < ec[i].pid.length; p++) {
	
	                    // Find protocol with the given id in the list of protocols
	                    for (var j = 0; j < privileges.length; j++) {
	                    	//console.log('hi');
	                        if (privileges[j].pid === ec[i].pid[p]) {
	                            // And replace the id with the actual object
	                            ec[i].pid[p] = privileges[j];
	                            break;
	                        }
	                    }
	                }
	                
	                for (var p = 0; p < ec[i].fid.length; p++) {
	
	                    // Find protocol with the given id in the list of protocols
	                    for (var j = 0; j < features.length; j++) {
	                    	//console.log('hi');
	                        if (features[j].fid === ec[i].fid[p]) {
	                            // And replace the id with the actual object
	                            ec[i].fid[p] = features[j];
	                            break;
	                        }
	                    }
	                }
	                
	            }
	            return ec;
		
		
	}
	factory.popRolePrivilegeFeature = function(pfid) {
       // console.log(privilege.pid);
        pfid = (pfid -1);
        rolesprivilegesfeatures.splice(pfid, 1);;
    }
    return factory;

}
]);


