ums.factory('timelineService', function($rootScope) {

    var timelineService = {};

    timelineService.announceShowTimeline = function() {
        $rootScope.$broadcast('show-timeline');
    }
    timelineService.announceHideTimeline = function() {
        $rootScope.$broadcast('hide-timeline');
    }

    return timelineService;

});